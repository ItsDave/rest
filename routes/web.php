<?php

Route::any('/', function () {
    abort('403', 'You shouldn\'t be here');
});