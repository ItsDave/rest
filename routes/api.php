<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::any('/', function () {
    abort('403', 'You shouldn\'t be here');
});


Route::post('/login', 'AuthController@login')->name('login');

/**
 * These routes are protected and require a valid JWT token
 * to access
 */
Route::middleware('jwt.auth')->group(function () {
    Route::get('/me', 'UserController@me')->name('me');

    // I could have used the laravel resource routing here, but to be honest I find it a bit "black box"
    // sometimes being verbose is a good thing :)
    Route::get('/users', 'UserController@list')->name('user.list');
    Route::post('/user/create', 'UserController@create')->name('user.create');
    Route::patch('/user/update', 'UserController@update')->name('user.update');
    Route::delete('/user/delete', 'UserController@delete')->name('user.delete');
});
