<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A lot of these tests are covered by Eloquents tests so are not
     * required. However, given the simplistic nature of the application
     * i've included them to show though process.
     */
    public function setUp()
    {
        parent::setUp();

        User::create([
            'name' => 'Dave Potts',
            'password' => 'testing',
            'email' => 'test@itsdave.co.uk',
        ]);
    }

    /**
     * Make sure that we can create a user
     * @test
     */
    public function can_create_user()
    {
        $user = User::create([
            'name' => 'Dave Potts',
            'password' => 'testing',
            'email' => 'dave@itsdave2.co.uk',
        ]);

        $this->assertNotEmpty($user);
    }

    /**
     * Ensure that we can update the users details.
     * @test
     */
    public function can_update_user()
    {
        $user = User::whereEmail('test@itsdave.co.uk')->first();
        $user->name = 'Joe Bloggs';

        $this->assertTrue($user->save());
    }

    /**
     * Ensure that we can delete a model from the ORM.
     * @test
     */
    public function can_delete_user()
    {
        $user = User::whereEmail('test@itsdave.co.uk')->delete();
        $this->assertEquals(1, $user);
    }

    /**
     * Ensure that we can get a list of users in alphabetical order
     * @test
     */
    public function can_get_list_of_users()
    {
        $users = User::getList();

        $this->assertNotEmpty($users);
    }
}
