<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use JWTAuth;

class ApiTest extends TestCase
{
    use RefreshDatabase;

    /**
     * JWT for testing authenticated calls
     * @var string
     */
    private $token = '';

    /**
     * Set up the user account for the test
     */
    public function setUp()
    {
        parent::setUp();

        $user = User::create([
            'name' => 'Dave Potts',
            'email' => 'dave@itsdave.co.uk',
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        ]);

        $this->token = JWTAuth::fromUser($user);
    }

    /**
     * Ensure that we are able to generate a JWT from user credentials.
     * @test
     */
    public function can_user_login_with_valid_credentials()
    {
        $credentials = [
            'email' => 'dave@itsdave.co.uk',
            'password' => 'secret',
        ];

        $response = $this->json('POST', route('login'), $credentials);

        $response->assertJsonStructure(['token']);
        $response->assertStatus(200);
    }

    /**
     * Ensure that we are able to generate a JWT from user credentials.
     * @test
     */
    public function ensure_user_cant_login_with_invalid_credentials()
    {
        $credentials = [
            'email' => 'dave@itsdave.co.uk',
            'password' => 'notvalidpassword',
        ];

        $response = $this->json('POST', route('login'), $credentials);

        $response->assertJsonStructure(['error']);
        $response->assertStatus(401);
    }

    /**
     * Ensure that if we make a request with a valid json token that
     * we can then return all of the users information.
     * @test
     */
    public function can_make_authorised_request()
    {
        $response = $this->json('GET', route('me', ['token' => $this->token]));

        $response->assertJsonStructure(['user']);
        $response->assertStatus(200);
    }

    /**
     * Ensure that an authenticated user can create another user
     * within the system
     * @test
     */
    public function can_authorised_user_create_user()
    {
        $newUser = [
            'name' => 'Joe Bloggs',
            'email' => 'test@testing.com',
            'password' => 'TabsVsSpaces?',
        ];

        $response = $this->json('POST', route('user.create', ['token' => $this->token]), $newUser);

        $response->assertJsonStructure(['name']);
        $response->assertStatus(200);
    }

    /**
     * Ensure that an authenticated user can update another user
     * within the system given their id.
     * @test
     */
    public function can_authorised_user_update_user()
    {
        $newDetails = [
            'id' => 1,
            'name' => 'Joe Bloggs',
        ];

        $response = $this->json('PATCH', route('user.update', ['token' => $this->token]), $newDetails);

        $response->assertJsonStructure(['user']);
        $response->assertSeeText('Joe Bloggs');
        $response->assertStatus(200);

    }

    /**
     * Ensure that an authenticated user can update another user
     * within the system given their id.
     * @test
     */
    public function can_authorised_user_delete_user()
    {
        $removeUser = [
            'id' => 1,
        ];

        $response = $this->json('DELETE', route('user.delete', ['token' => $this->token]), $removeUser);

        $response->assertJsonStructure(['success']);
        $response->assertStatus(200);

    }

    /**
     * Ensure that an authenticated user can update another user
     * within the system given their id.
     * @test
     */
    public function cant_delete_a_user_that_doesnt_exist()
    {
        $removeUser = [
            'id' => 999,
        ];

        $response = $this->json('DELETE', route('user.delete', ['token' => $this->token]), $removeUser);

        $response->assertJsonStructure(['error']);
        $response->assertStatus(400);

    }

    /**
     * Ensure that an authenticated user can create another user
     * within the system
     * @test
     */
    public function ensure_new_user_validation_triggers_on_missing_fields()
    {
        $newUser = [
            'name' => 'Joe Bloggs',
            'password' => 'TabsVsSpaces?',
        ];

        $response = $this->json('POST', route('user.create', ['token' => $this->token]), $newUser);

        $response->assertJsonStructure(['message']);
        $response->assertStatus(422);
    }

    /**
     * Make sure we can return a JSON list of all the users within the system
     * @test
     */
    public function can_authorised_user_retrieve_list_of_users()
    {
        $response = $this->json('GET', route('user.list', ['token' => $this->token]));

        $response->assertJsonCount(1);
        $response->assertStatus(200);
    }

    /**
     * Make sure if we make a request to a protected route without a token
     * supplied that it returns and error and a 400 status.
     * @test
     */
    public function cant_make_request_to_protected_route_without_token()
    {
        $response = $this->json('GET', route('me'));

        $response->assertJsonStructure(['error']);
        $response->assertStatus(400);
    }

    /**
     * Ensure that if we make a request with an invalid json token that
     * it returns a 400 status and an error
     * @test
     */
    public function cant_make_authorised_request_with_invalid_token()
    {
        $response = $this->json('GET', route('me', ['token' => 'invalid_token']));

        $response->assertJsonStructure(['error']);
        $response->assertStatus(400);
    }
}
