<?php

use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 50)->create();

        // Create me a test account
        factory(User::class)->create([
            'email' => 'dave@itsdave.co.uk',
        ]);
    }
}
