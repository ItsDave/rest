<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Psy\Util\Json;

class UserController extends Controller
{
    /**
     * Return the currently logged in user.
     * @return JsonResponse
     */
    public function me() : JsonResponse
    {
        return response()->json($this->getCurrentUser());
    }

    /**
     * Return a list of users in the system
     * @return JsonResponse
     */
    public function list() : JsonResponse
    {
        $users = User::getList();
        return response()->json($users);
    }

    /**
     * Create a new user based on the information provided
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request) : JsonResponse
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'password' => "required|min:8",
        ]);

        $user = User::create($request->all());
        return response()->json($user);
    }

    /**
     * Update a user record
     * @param Request $request
     * @return JsonResponse
     */
    public function update(Request $request) : JsonResponse
    {
        $this->validate($request, [
            'id' => 'required',
            'email' => "email",
            'name' => 'max:255',
        ]);

        $user = User::findOrFail($request->get('id'));
        $user->fill($request->only(['name', 'email']));

        $user->save();

        return response()->json(['success' => 'user updated', 'user' => $user]);
    }

    /**
     * Given a user id, delete that user from the database.
     * Ideally this should be using soft deletes but you get the idea.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function delete(Request $request) : JsonResponse
    {
        $this->validate($request, [
            'id' => 'required|int',
        ]);

        $user = User::destroy($request->get('id'));

        if ($user == 1) {
            return response()->json(['success' => 'User deleted']);
        }

        return response()->json(['error' => 'User not found'], 400);
    }
}
