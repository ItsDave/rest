<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Return a list of all the users in alphabetical order.
     * @return Collection|static[]
     */
    public static function getList() : Collection
    {
        return self::orderBy('name')->get();
    }
    
    /**
     * hash the password.
     * @return void
     */
    public function setPasswordAttribute($value) : void
    {
        $this->attributes['password'] = Hash::make($value);
    }
}
